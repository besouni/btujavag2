package btu;

import javax.swing.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server implements Runnable{
    Socket socket;
    ObjectInputStream objectInputStream;
    String message;
    ObjectOutputStream objectOutputStream;

    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(8008);
            while (true) {
                socket = serverSocket.accept();
                objectInputStream = new ObjectInputStream(socket.getInputStream());
                message = (String) objectInputStream.readObject();
                System.out.println("Client say: " + message);
                Scanner scanner = new Scanner(System.in);
//                message = scanner.nextLine();
                message = JOptionPane.showInputDialog("Server:");
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                objectOutputStream.writeObject(message);
                if(message.equals("bye")){
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
