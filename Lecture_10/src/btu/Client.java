package btu;

import javax.swing.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client extends Thread {
    Socket socket;
    ObjectOutputStream objectOutputStream;
    String message;
    ObjectInputStream objectInputStream;

    @Override
    public void run() {
        try {
            while (true) {
                socket = new Socket(InetAddress.getByName("localhost"), 8008);
                Scanner scanner = new Scanner(System.in);
//                message = scanner.nextLine();
                message = JOptionPane.showInputDialog("Client: ");
                objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                objectOutputStream.writeObject(message);
                objectInputStream = new ObjectInputStream(socket.getInputStream());
                message = (String) objectInputStream.readObject();
                System.out.println("Server say: " + message);
                if(message.equals("bye")){
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
