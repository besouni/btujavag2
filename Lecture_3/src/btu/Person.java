package btu;

public class Person {
    String name = "Ana";
    String lastname;
    int age;
    String id;


    public Person() {

    }

    int returnAfterAge(){
//        System.out.println(age+5);
        return age+5;
    }

    int returnAfterAge(int years){
        return age+years;
    }

    int returnAfterAge(int years1, int years2){
        return age+years1+years2;
    }

    float returnAfterAge(float years1, int years2){
        return (float) (age+years1+years2+2.5);
    }

    float returnAfterAge(int years1, float years2){
        return (float) (age+years1+years2+2.5);
    }




    void classProperties(){
        System.out.println("====");
        System.out.println(name);
        System.out.println(lastname);
        System.out.println(age);
        System.out.println(id);
        System.out.println("====");
    }

    @Override
    public String toString() {
        return "Person{" +
                "name:'" + name + '\'' +
                ", lastname:'" + lastname + '\'' +
                ", age:" + age +
                ", id:'" + id + '\'' +
                '}';
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setId(String _id) {
        id = _id;
    }
}
