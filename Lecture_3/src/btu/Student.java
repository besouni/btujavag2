package btu;

public class Student {
    double gpi;
    String gender;
    boolean status;
    int course;

    public Student() {
        System.out.println("Constructor is running....");
    }

    public Student(double gpi) {
        this.gpi = gpi;
    }

    public Student(double gpi, boolean status) {
        this.gpi = gpi;
        this.status = status;
    }
}
