package btu;

public class Main {

    public static void main(String[] args) {
	// write your code here
//        System.out.println("Main 1");
//        Person p1 = new Person();
//        p1.lastname = "Modebadze";
//        p1.classProperties();
//        System.out.println(p1.toString());
//        p1.age = 8;
//        System.out.println(p1.returnAfterAge());
//        System.out.println(p1.returnAfterAge(8));
//        System.out.println(p1.returnAfterAge(1, 9));
        Student s1 = new Student();
        Student s2 = new Student(3.5);
        test2 t2_1 = new test2();
        System.out.println(t2_1.toString());
        test3 t3_1 = new test3();
        System.out.println(t3_1.toString());
    }
}

final class  test3{
    int f = 56;

    @Override
    public String toString() {
        return "test3{" +
                "f=" + f +
                '}';
    }
}


abstract class test1{
    int t = 10;

    @Override
    public String toString() {
        return "test1{" +
                "t=" + t +
                '}';
    }
}

class test2 extends test1{

}
