package btu;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int x  = 9, y = 1, z = 0;
        int [] m = {4, 5, 3};

        System.out.println("Before Exception");

        try{
            z = x/y;
            System.out.println("Try -> "+(x/y));
            System.out.println(m[17]);
        } catch (ArithmeticException e){
            z = x/(y+1);
            System.out.println("Catch -> "+(x/(y+1)));
            System.out.println(m[m.length-1]);
        }


        System.out.println("Z = "+z);
        System.out.println("After Exception");


//        Thread.sleep(2000);
//        System.out.println(x);
    }
}
