package btu;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class HomeView implements Initializable {
    @FXML
    public VBox container;

    public void insert(){
        try {
            Stage  stage = new Stage();
            stage.setTitle("მონაცემების ჩაწერა");
            Parent parent = null;
            parent = FXMLLoader.load(getClass().getResource("InsertView.fxml"));
            Scene scene = new Scene(parent);
            stage.setScene(scene);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        DbControlerr db = new DbControlerr();
        ResultSet result = db.select();
        try {
            while (result.next()){
                Label username = new Label(result.getString("username"));
                container.getChildren().add(username);
                container.setSpacing(20);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
