package btu;

public class Test <GPA, ID> {
    Object T1;
    Object T2;
    GPA g1;
    ID id;

    public void test1_1(Integer time){
        System.out.println(time.getClass().getTypeName());
    }

    public void test1_2(Object time){
        System.out.println(time.getClass().getTypeName());
    }

    public <DRO> void test1_3(DRO t){
        System.out.println(t.getClass().getTypeName());
    }
}
