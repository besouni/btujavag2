package testthread2;

public class Thread1 implements Runnable {
    MyResource myResource;

    public Thread1(MyResource myResource){
        this.myResource = myResource;
    }

    @Override
    public void run() {
        myResource.changeN(200, 5, "Thread1");
    }
}
